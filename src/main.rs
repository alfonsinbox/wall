use std::{error::Error, fs::create_dir_all, path::Path};

use clap::{Parser, ValueEnum};
use regex::Regex;
use roux::{Subreddit, response::BasicThing, submission::SubmissionData};

#[derive(Parser, Debug)]
#[command(author, about, version = "1.0")]
struct Opts {
    #[arg(short, long, default_value = "wallpaper")]
    subreddit: String,

    #[arg(short, long)]
    debug: bool,

    #[arg(short, long, value_enum, default_value = "hot")]
    order: Ordering,
}

#[derive(ValueEnum, Debug, Clone)]
enum Ordering {
    Hot,
    Top,
    New,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let opts = Opts::parse();
    if opts.debug {
        println!("Running with options {:#?}", opts);
    }

    let top_url = get_first_image(&opts).await?;
    if opts.debug {
        println!("Got url {:#?}", top_url);
    }

    let response = reqwest::get(&top_url).await?;
    if opts.debug {
        println!("Downloaded image with status {:#?}", response.status());
    }

    let reddit_filename = response.url().path_segments().unwrap().last().unwrap();
    let filename = format!("{}-{}", opts.subreddit, reddit_filename);

    let bytes = &response.bytes().await?;
    let imresults = image::load_from_memory(bytes)?;
    if opts.debug {
        println!("Loaded image from response");
    }

    let save_dir = dirs::picture_dir().unwrap().join(Path::new("WallPeh"));
    if opts.debug {
        println!("Saving file to {}", save_dir.to_str().unwrap());
    }
    create_dir_all(&save_dir)?;
    let full_path = save_dir.join(&filename);
    imresults.save(&full_path)?;
    if opts.debug {
        println!("File saved");
    }

    wallpaper::set_from_path(full_path.to_str().unwrap())?;
    if opts.debug {
        println!("Set wallpaper");
    }

    Ok(())
}

async fn get_first_image(opts: &Opts) -> Result<String, Box<dyn Error>> {
    let sub = Subreddit::new(&opts.subreddit);
    let results = match opts.order {
        Ordering::Hot => sub.hot(25, None).await,
        Ordering::Top => sub.top(25, None).await,
        Ordering::New => sub.latest(25, None).await,
    }?;
    let top_submission = results
        .data
        .children
        .iter()
        .find(|thing| is_image_url(thing, opts.debug))
        .unwrap();
    Ok(top_submission.data.url.to_owned().unwrap())
}

fn is_image_url(thing: &BasicThing<SubmissionData>, debug: bool) -> bool {
    if debug {
        println!("{thing:#?}");
    }
    if let Some(url) = &thing.data.url {
        Regex::new(r"\.(jpg|png)").unwrap().is_match(url)
    } else {
        false
    }
}
